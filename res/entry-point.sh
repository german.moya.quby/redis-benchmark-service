#!/bin/sh

REDIS_HOST=${REDIS_HOST:-redis}
REDIS_PORT=${REDIS_PORT:-6379}
PORT=${PORT:-8080}

/usr/bin/time -f '# Executed in %e seconds' -o /tmp/benchmark.time \
redis-benchmark -h $REDIS_HOST -p $REDIS_PORT -r 1000 -t set,get --csv \
> /tmp/benchmark.data

cat /tmp/benchmark.time /tmp/benchmark.data > /tmp/www/benchmark.txt

darkhttpd /tmp/www --port $PORT
