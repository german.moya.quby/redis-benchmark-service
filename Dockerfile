FROM alpine
RUN apk add redis

FROM alpine
COPY --from=0 /usr/bin/redis-benchmark /usr/bin
RUN mkdir /tmp/www &&\
    apk --no-cache add darkhttpd
COPY res/index.html /tmp/www
COPY  res/entry-point.sh /
ENTRYPOINT /entry-point.sh
